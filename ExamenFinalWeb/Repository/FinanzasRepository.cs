﻿using ExamenFinalWeb.DB;
using ExamenFinalWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinalWeb.Repository
{
    public interface IFinanzasRepository
    {
        public Cuenta findCuentaByNombre(string name);
        public List<Cuenta> GetAllCuentas();
        public List<Gasto> GetAllGasto();
        public List<Ingreso> GetAllIngreso();
        public void createCuenta(Cuenta cuenta);
        public void createGasto(Gasto gasto);
        public void createIngreso(Ingreso ingreso);
        public double calculaMontoConIngreso(double montoC, double montOp);
        public double calculaMontoConGasto(double montoC, double montOp);
    }

    public class FinanzasRepository : IFinanzasRepository
    {
        private readonly FinalContext context;
        public FinanzasRepository(FinalContext _context)
        {
            this.context = _context;
        }

        public double calculaMontoConGasto(double montoC, double montOp)
        {
            return montoC - montOp;
        }

        public double calculaMontoConIngreso(double montoC, double montOp)
        {
            return montoC + montOp;
        }

        public void createCuenta(Cuenta cuenta)
        {
            context.Cuentas.Add(cuenta);
            context.SaveChanges();
        }

        public void createGasto(Gasto gasto)
        {
            context.Gastos.Add(gasto);
            context.SaveChanges();
        }

        public void createIngreso(Ingreso ingreso)
        {
            context.Ingresos.Add(ingreso);
            context.SaveChanges();
        }

        public Cuenta findCuentaByNombre(string name)
        {
            return context.Cuentas.FirstOrDefault(c => c.Nombre == name);
        }

        public List<Cuenta> GetAllCuentas()
        {
            return context.Cuentas.ToList();
        }

        public List<Gasto> GetAllGasto()
        {
            return context.Gastos.ToList();
        }

        public List<Ingreso> GetAllIngreso()
        {
            return context.Ingresos.ToList();
        }
    }
}
