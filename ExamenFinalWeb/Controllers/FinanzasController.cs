﻿using ExamenFinalWeb.Models;
using ExamenFinalWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinalWeb.Controllers
{
    public class FinanzasController : Controller
    {
        private readonly IFinanzasRepository repository;
        public FinanzasController(IFinanzasRepository _repository)
        {
            this.repository = _repository;
        }

        public IActionResult Finanzas()
        {
            List<Cuenta> cuentas = repository.GetAllCuentas();
            return View("Finanzas", cuentas);
        }
        public IActionResult CrearCuenta()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CrearCuenta(Cuenta cuenta)
        {
            if(cuenta.Categoria=="" || cuenta.Nombre=="" || cuenta.Saldo<=0)
                return RedirectToAction("CrearCuenta");
            else
            repository.createCuenta(cuenta);
            return RedirectToAction("CrearCuenta");
        }
        public IActionResult MuestraGastos()
        {
            var lgastos = repository.GetAllGasto();
            var cuentas = repository.GetAllCuentas();
            List<Gasto> gastos = new List<Gasto>();
            foreach (Cuenta c in cuentas)
            {
                foreach (Gasto gasto in lgastos)
                {
                    if (gasto.Cuenta == c.Nombre)
                        gastos.Add(gasto);
                }
            }
            return View("MuestraGastos", gastos);
        }
        [HttpGet]
        public IActionResult RegistraGasto()
        {
            List<Cuenta> cuentas = repository.GetAllCuentas();
            return View("RegistraGasto", cuentas);
        }
        [HttpPost]
        public IActionResult RegistraGasto(Gasto gasto)
        {
            if (gasto.Cuenta == "" || gasto.Cuenta == "" || gasto.Monto <= 0 || gasto.Descripcion == "")
            {
                List<Cuenta> cuentas = repository.GetAllCuentas();
                return RedirectToAction("RegistraGasto",cuentas);
            }

            var cuenta = repository.findCuentaByNombre(gasto.Cuenta);
            if (gasto.Monto > cuenta.Saldo)
            {
                ModelState.AddModelError("El monto no puede ser mayor al saldo de la cuenta. Saldo de la cuenta", cuenta.Saldo.ToString());
                return View();
            }
            cuenta.Saldo = repository.calculaMontoConGasto(cuenta.Saldo, gasto.Monto);

            gasto.FechaRegistro = new DateTime();
            repository.createGasto(gasto);
            return RedirectToAction("Finanzas");
        }
        [HttpGet]
        public IActionResult MuestraIngresos()
        {
            var cuentas = repository.GetAllCuentas();
            var lingresos = repository.GetAllIngreso();
            List<Ingreso> ingresos = new List<Ingreso>();

            foreach (Cuenta c in cuentas)
            {
                foreach (Ingreso ingreso in lingresos)
                {
                    if (ingreso.Cuenta == c.Nombre)
                        ingresos.Add(ingreso);
                }
            }
            return View("MuestraIngresos", ingresos);
        }
        //ingresos
        [HttpGet]
        public IActionResult CreaIngreso()
        {
            var Cuentas = repository.GetAllCuentas();
            return View("CreaIngreso", Cuentas);
        }
        [HttpPost]
        public IActionResult CreaIngreso(Ingreso ingreso)
        {

            if (ingreso.Cuenta == "" || ingreso.Cuenta == "" || ingreso.Monto <= 0 || ingreso.Descripcion == "")
            {   
                List<Cuenta> cuentas = repository.GetAllCuentas();
                return RedirectToAction("CreaIngreso", cuentas);
            }


            ingreso.FechaIngreso = new DateTime();
            var cuenta = repository.findCuentaByNombre(ingreso.Cuenta);

            cuenta.Saldo = repository.calculaMontoConIngreso(cuenta.Saldo, ingreso.Monto);

            repository.createIngreso(ingreso);
            return RedirectToAction("Finanzas");
        }

    }
}
