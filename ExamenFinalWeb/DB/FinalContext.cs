﻿using ExamenFinalWeb.BDD.Mapping;
using ExamenFinalWeb.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinalWeb.DB
{
    public class FinalContext : DbContext
    {

        public virtual DbSet<Cuenta> Cuentas { get; set; }
        public virtual DbSet<Gasto> Gastos { get; set; }
        public virtual DbSet<Ingreso> Ingresos { get; set; }


        public FinalContext(DbContextOptions<FinalContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new CuentaMap());
            modelBuilder.ApplyConfiguration(new GastoMap());
            modelBuilder.ApplyConfiguration(new IngresoMap());
            /*
            modelBuilder.ApplyConfiguration(new CursoMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());

            modelBuilder.ApplyConfiguration(new ModuloMap());

            modelBuilder.ApplyConfiguration(new RegCursosUsuariosMap());
            modelBuilder.ApplyConfiguration(new TemaMap());
            modelBuilder.ApplyConfiguration(new ObjetivoMap());
            modelBuilder.ApplyConfiguration(new ExamenMap());
            modelBuilder.ApplyConfiguration(new AlternativasMap());
            modelBuilder.ApplyConfiguration(new ProgresoTemaUserMap());
            modelBuilder.ApplyConfiguration(new ComentarioMap());
            modelBuilder.ApplyConfiguration(new ComentarioCMap());
            modelBuilder.ApplyConfiguration(new ProgresoCMap());
            */

        }
    }
}
