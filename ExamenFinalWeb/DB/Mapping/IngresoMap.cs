﻿using ExamenFinalWeb.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExamenFinalWeb.BDD.Mapping
{
    public class IngresoMap :IEntityTypeConfiguration<Ingreso>
    {
        public void Configure(EntityTypeBuilder<Ingreso> builder)
        {
            builder.ToTable("Ingreso");
            builder.HasKey(g => g.Id);
        }
    }
}
