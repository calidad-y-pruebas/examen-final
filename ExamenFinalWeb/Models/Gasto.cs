﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinalWeb.Models
{
    public class Gasto
    {
        public int Id { get; set; }
        public int IdCuenta { get; set; }
        public string Cuenta { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Descripcion { get; set; }
        public double Monto { get; set; }
    }
}
