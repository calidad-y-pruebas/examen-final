﻿using ExamenFinalTest.Mock;
using ExamenFinalWeb.Controllers;
using ExamenFinalWeb.Models;
using ExamenFinalWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenFinalTest.ControllerTest
{
    public class FinanzasControllerTest
    {
        private Mock<IFinanzasRepository> repository;
        [SetUp]
        public void SetUp()
        {
            repository = new Mock<IFinanzasRepository>();
        }

        [Test]
        public void TestCrearNuevaCuentaPasa()
        {

            var serviceMock = new Mock<IFinanzasRepository>();


            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.CrearCuenta (new Cuenta { Id = 1, Nombre = "Cuenta 1", Saldo = 200, Categoria = "Propio" });

            Assert.IsInstanceOf<RedirectToActionResult>(redirect);
        }
        [Test]
        public void TestRegistrarGastoPasa()
        {

            var serviceMock = new Mock<IFinanzasRepository>();

            var cuenta = new Cuenta { Id = 1, Nombre = "Cuenta 1", Saldo = 200, Categoria = "Propio" };
            serviceMock.Setup(o => o.findCuentaByNombre("Cuenta 1")).Returns(cuenta);

            var controller = new FinanzasController(serviceMock.Object);
            DateTime fech = DateTime.Now;
            var redirect = controller.RegistraGasto(new Gasto { Id = 1, IdCuenta = 0, Cuenta = "Cuenta 1", Monto = 60, Descripcion = "Apuestas", FechaRegistro = fech });

            Assert.IsInstanceOf<RedirectToActionResult>(redirect);
        }
        [Test]
        public void TestCrearGastoNoPasa()
        {

            var serviceMock = new Mock<IFinanzasRepository>();
            var cuenta = new Cuenta { Id = 1, Nombre = "Cuenta 1", Saldo = 200, Categoria = "Propio" };
            serviceMock.Setup(o => o.findCuentaByNombre("Cuenta 1")).Returns(cuenta);

            List<Cuenta> cuentas = new List<Cuenta>
            {
                new Cuenta{Id = 1, Nombre="Cuenta 1",Saldo = 200,Categoria="Propio"},
                new Cuenta{Id = 2, Nombre="Cuenta 2",Saldo = 400,Categoria="Crédito"},
                new Cuenta{Id = 3, Nombre="Cuenta 3",Saldo = 600,Categoria="Propio"},
                new Cuenta{Id = 4, Nombre="Cuenta 4",Saldo = 1000,Categoria="Crédito"},
                new Cuenta{Id = 5, Nombre="Cuenta 5",Saldo = 100,Categoria="Crédito"},
                new Cuenta{Id = 6, Nombre="Cuenta 6",Saldo = 200,Categoria="Propio"},
                new Cuenta{Id = 7, Nombre="Cuenta 7",Saldo = 350,Categoria="Crédito"}
            };
            serviceMock.Setup(o => o.GetAllCuentas()).Returns(cuentas);

            var controller = new FinanzasController(serviceMock.Object);
            DateTime fech = DateTime.Now;
            var redirect = controller.RegistraGasto(new Gasto { Id = 1, IdCuenta = 0, Cuenta = "Cuenta 1", Monto = 60, Descripcion = "Apuestas", FechaRegistro = fech });

            Assert.IsInstanceOf<RedirectToActionResult>(redirect);
        }
        [Test]
        public void TestCrearIngresoPasa()
        {

            var serviceMock = new Mock<IFinanzasRepository>();
            var cuenta = new Cuenta { Id = 1, Nombre = "Cuenta 1", Saldo = 200, Categoria = "Propio" };
            serviceMock.Setup(o => o.findCuentaByNombre("Cuenta 1")).Returns(cuenta);

            var controller = new FinanzasController(serviceMock.Object);
            DateTime fech = DateTime.Now;
            var ingreso = new Ingreso { Id = 1, IdCuenta = 0, Cuenta = "Cuenta 1", Monto = 60, Descripcion = "Apuestas", FechaIngreso = fech };
            var redirect = controller.CreaIngreso(ingreso);

            Assert.IsInstanceOf<RedirectToActionResult>(redirect);
        }
        [Test]
        public void TestCrearIngresoNoPasa()
        {

            var serviceMock = new Mock<IFinanzasRepository>();
            var cuenta = new Cuenta { Id = 1, Nombre = "Cuenta 1", Saldo = 200, Categoria = "Propio" };
            serviceMock.Setup(o => o.findCuentaByNombre("Cuenta 1")).Returns(cuenta);

            List<Cuenta> cuentas = new List<Cuenta>
            {
                new Cuenta{Id = 1, Nombre="Cuenta 1",Saldo = 200,Categoria="Propio"},
                new Cuenta{Id = 2, Nombre="Cuenta 2",Saldo = 400,Categoria="Crédito"},
                new Cuenta{Id = 3, Nombre="Cuenta 3",Saldo = 600,Categoria="Propio"},
                new Cuenta{Id = 4, Nombre="Cuenta 4",Saldo = 1000,Categoria="Crédito"},
                new Cuenta{Id = 5, Nombre="Cuenta 5",Saldo = 100,Categoria="Crédito"},
                new Cuenta{Id = 6, Nombre="Cuenta 6",Saldo = 200,Categoria="Propio"},
                new Cuenta{Id = 7, Nombre="Cuenta 7",Saldo = 350,Categoria="Crédito"}
            };
            serviceMock.Setup(o => o.GetAllCuentas()).Returns(cuentas);

            var controller = new FinanzasController(serviceMock.Object);
            DateTime fech = DateTime.Now;
            var ingreso = new Ingreso { Id = 1, IdCuenta = 0, Cuenta = "", Monto = 60, Descripcion = "", FechaIngreso = fech };
            var redirect = controller.CreaIngreso(ingreso);

            Assert.IsInstanceOf<RedirectToActionResult>(redirect);
            
        }
        [Test]
        public void TestMuestraIngresos()
        {

            var serviceMock = new Mock<IFinanzasRepository>();

            List<Cuenta> cuentas = new List<Cuenta>
            {
                new Cuenta{Id = 1, Nombre="Cuenta 1",Saldo = 200,Categoria="Propio"},
                new Cuenta{Id = 2, Nombre="Cuenta 2",Saldo = 400,Categoria="Crédito"},
                new Cuenta{Id = 3, Nombre="Cuenta 3",Saldo = 600,Categoria="Propio"},
                new Cuenta{Id = 4, Nombre="Cuenta 4",Saldo = 1000,Categoria="Crédito"},
                new Cuenta{Id = 5, Nombre="Cuenta 5",Saldo = 100,Categoria="Crédito"},
                new Cuenta{Id = 6, Nombre="Cuenta 6",Saldo = 200,Categoria="Propio"},
                new Cuenta{Id = 7, Nombre="Cuenta 7",Saldo = 350,Categoria="Crédito"}
            };
            DateTime fech = DateTime.Now;
            List<Ingreso> ingresos = new List<Ingreso>
            {
                new Ingreso{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaIngreso=fech},
                new Ingreso{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaIngreso=fech},
                new Ingreso{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaIngreso=fech},
                new Ingreso{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaIngreso=fech},
                new Ingreso{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaIngreso=fech},
                new Ingreso{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaIngreso=fech},
            };

            serviceMock.Setup(o => o.GetAllCuentas()).Returns(cuentas);
            serviceMock.Setup(o => o.GetAllIngreso()).Returns(ingresos);


            var controller = new FinanzasController(serviceMock.Object);
            var result = controller.MuestraIngresos() as ViewResult;

            Assert.IsInstanceOf<ViewResult>(result);
            //Assert.AreEqual("MuestraIngresos", result.ViewName);
        }
        [Test]
        public void TestMuestraGastos()
        {

            var serviceMock = new Mock<IFinanzasRepository>();
            var cuenta = new Cuenta { Id = 1, Nombre = "Cuenta 1", Saldo = 200, Categoria = "Propio" };

            List<Cuenta> cuentas = new List<Cuenta>
            {
                new Cuenta{Id = 1, Nombre="Cuenta 1",Saldo = 200,Categoria="Propio"},
                new Cuenta{Id = 2, Nombre="Cuenta 2",Saldo = 400,Categoria="Crédito"},
                new Cuenta{Id = 3, Nombre="Cuenta 3",Saldo = 600,Categoria="Propio"},
                new Cuenta{Id = 6, Nombre="Cuenta 6",Saldo = 200,Categoria="Propio"},
                new Cuenta{Id = 7, Nombre="Cuenta 7",Saldo = 350,Categoria="Crédito"}
            };
            DateTime fech = DateTime.Now;
            List<Gasto> gastos = new List<Gasto>
            {
                new Gasto{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaRegistro=fech},
                new Gasto{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaRegistro=fech},
                new Gasto{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaRegistro=fech},
                new Gasto{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaRegistro=fech},
            };

            serviceMock.Setup(o => o.GetAllCuentas()).Returns(cuentas);
            serviceMock.Setup(o => o.GetAllGasto()).Returns(gastos);


            var controller = new FinanzasController(serviceMock.Object);
            var result = controller.MuestraGastos() as ViewResult;

            Assert.IsInstanceOf<ViewResult>(result);
            Assert.AreEqual("MuestraGastos", result.ViewName);
        }
        [Test]
        public void TestMuestraFinanzas()
        {

            var serviceMock = new Mock<IFinanzasRepository>();

            List<Cuenta> cuentas = new List<Cuenta>
            {
                new Cuenta{Id = 1, Nombre="Cuenta 1",Saldo = 200,Categoria="Propio"},
                new Cuenta{Id = 2, Nombre="Cuenta 2",Saldo = 400,Categoria="Crédito"},
                new Cuenta{Id = 3, Nombre="Cuenta 3",Saldo = 600,Categoria="Propio"},
            };
            serviceMock.Setup(o => o.GetAllCuentas()).Returns(cuentas);

            var controller = new FinanzasController(serviceMock.Object);
            DateTime fech = DateTime.Now;
            var result = controller.Finanzas() as ViewResult;

            Assert.IsInstanceOf<ViewResult>(result);
        }
    }   
}
