﻿using ExamenFinalWeb.DB;
using ExamenFinalWeb.Models;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamenFinalTest.Mock
{
    public static class AppContextMock
    {
        public static Mock<FinalContext> GetApplicationContext()
        {
            IQueryable<Cuenta> cuentaData = GetCuentasData();
            IQueryable<Gasto> gastosData = GetGastoData();
            IQueryable<Ingreso> ingresosData = GetIngresoData();

            var mockDbSetCuenta = new Mock<DbSet<Cuenta>>();
            mockDbSetCuenta.As<IQueryable<Cuenta>>().Setup(m => m.Provider).Returns(cuentaData.Provider);
            mockDbSetCuenta.As<IQueryable<Cuenta>>().Setup(m => m.Expression).Returns(cuentaData.Expression);
            mockDbSetCuenta.As<IQueryable<Cuenta>>().Setup(m => m.ElementType).Returns(cuentaData.ElementType);
            mockDbSetCuenta.As<IQueryable<Cuenta>>().Setup(m => m.GetEnumerator()).Returns(cuentaData.GetEnumerator());
            mockDbSetCuenta.Setup(m => m.AsQueryable()).Returns(cuentaData);

            var mockDbSetGasto = new Mock<DbSet<Gasto>>();
            mockDbSetGasto.As<IQueryable<Gasto>>().Setup(m => m.Provider).Returns(gastosData.Provider);
            mockDbSetGasto.As<IQueryable<Gasto>>().Setup(m => m.Expression).Returns(gastosData.Expression);
            mockDbSetGasto.As<IQueryable<Gasto>>().Setup(m => m.ElementType).Returns(gastosData.ElementType);
            mockDbSetGasto.As<IQueryable<Gasto>>().Setup(m => m.GetEnumerator()).Returns(gastosData.GetEnumerator());
            mockDbSetGasto.Setup(m => m.AsQueryable()).Returns(gastosData);

            var mockDbSetIngreso = new Mock<DbSet<Ingreso>>();
            mockDbSetIngreso.As<IQueryable<Ingreso>>().Setup(m => m.Provider).Returns(ingresosData.Provider);
            mockDbSetIngreso.As<IQueryable<Ingreso>>().Setup(m => m.Expression).Returns(ingresosData.Expression);
            mockDbSetIngreso.As<IQueryable<Ingreso>>().Setup(m => m.ElementType).Returns(ingresosData.ElementType);
            mockDbSetIngreso.As<IQueryable<Ingreso>>().Setup(m => m.GetEnumerator()).Returns(ingresosData.GetEnumerator());
            mockDbSetIngreso.Setup(m => m.AsQueryable()).Returns(ingresosData);

            var mockContext = new Mock<FinalContext>(new DbContextOptions<FinalContext>());
            mockContext.Setup(c => c.Cuentas).Returns(mockDbSetCuenta.Object);
            mockContext.Setup(c => c.Gastos).Returns(mockDbSetGasto.Object);
            mockContext.Setup(c => c.Ingresos).Returns(mockDbSetIngreso.Object);

            return mockContext;
        }
        private static IQueryable<Cuenta> GetCuentasData()
            {
                return new List<Cuenta>
                        {
                            new Cuenta{Id = 1, Nombre="Cuenta 1",Saldo = 200,Categoria="Propio"},
                            new Cuenta{Id = 2, Nombre="Cuenta 2",Saldo = 400,Categoria="Crédito"},
                            new Cuenta{Id = 3, Nombre="Cuenta 3",Saldo = 600,Categoria="Propio"},
                            new Cuenta{Id = 4, Nombre="Cuenta 4",Saldo = 1000,Categoria="Crédito"},
                            new Cuenta{Id = 5, Nombre="Cuenta 5",Saldo = 100,Categoria="Crédito"},
                            new Cuenta{Id = 6, Nombre="Cuenta 6",Saldo = 200,Categoria="Propio"},
                            new Cuenta{Id = 7, Nombre="Cuenta 7",Saldo = 350,Categoria="Crédito"},
                        }.AsQueryable();
            }
        private static IQueryable<Gasto> GetGastoData()
        {
            DateTime fech = DateTime.Now;
            return new List<Gasto>
                            {
                                new Gasto{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaRegistro=fech},
                                new Gasto{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaRegistro=fech},
                                new Gasto{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaRegistro=fech},
                                new Gasto{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaRegistro=fech},
                                new Gasto{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaRegistro=fech},
                                new Gasto{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaRegistro=fech},
                            }.AsQueryable();
        }
        private static IQueryable<Ingreso> GetIngresoData()
        {
            DateTime fech = DateTime.Now;
            return new List<Ingreso>
                                {
                                    new Ingreso{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaIngreso=fech},
                                    new Ingreso{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaIngreso=fech},
                                    new Ingreso{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaIngreso=fech},
                                    new Ingreso{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaIngreso=fech},
                                    new Ingreso{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaIngreso=fech},
                                    new Ingreso{Id = 1,IdCuenta=0,Cuenta ="Cuenta 1",Monto = 60,Descripcion="Apuestas",FechaIngreso=fech},
                                }.AsQueryable();
        }
    }
}
