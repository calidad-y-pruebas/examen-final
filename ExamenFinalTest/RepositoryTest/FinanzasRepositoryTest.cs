﻿using ExamenFinalTest.Mock;
using ExamenFinalWeb.DB;
using ExamenFinalWeb.Models;
using ExamenFinalWeb.Repository;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenFinalTest.RepositoryTest
{
    class FinanzasRepositoryTest
    {
        private Mock<FinalContext> mockContext;

        [SetUp]
        public void Setup()
        {
            mockContext = AppContextMock.GetApplicationContext();
        }
        [Test]
        public void Test_BuscaCuentaPorNombre_DebeRetornarLaCuentaCorrecta()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.findCuentaByNombre("Cuenta 1");

            Assert.AreEqual("Cuenta 1", result.Nombre);
            Assert.AreEqual(200, result.Saldo);
        }
        [Test]
        public void Test_ObtenerTodasLasCuentas_DebeRetornarTodasLasCuentasCorrectas()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.GetAllCuentas();

            Assert.AreEqual(7, result.Count);
        }
        [Test]
        public void Test_ObtenerTodasLasCuentas_DebeRetornarTodasLasCuentasMasDelTotal()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.GetAllCuentas();

            Assert.AreNotEqual(17, result.Count);
        }
        [Test]
        public void Test_ObtenerTodasLasCuentas_DebeRetornarTodasLasCuentasMenosDelTotal()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.GetAllCuentas();

            Assert.AreNotEqual(3, result.Count);
        }
        [Test]
        public void Test_ObtenerTodasLosGastos_DebeRetornarTodosLosGastos()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.GetAllGasto();

            Assert.AreEqual(6, result.Count);
        }
        [Test]
        public void Test_ObtenerTodasLosGastos_DebeRetornarGastosMasDelTotal()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.GetAllGasto();

            Assert.AreNotEqual(30, result.Count);
        }
        [Test]
        public void Test_ObtenerTodasLosGastos_DebeRetornarGastosMenosDelTotal()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.GetAllGasto();

            Assert.AreNotEqual(1, result.Count);
        }
        [Test]
        public void Test_ObtenerTodasLosIngresos_DebeRetornarTodosLosIngresos()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.GetAllIngreso();

            Assert.AreEqual(6, result.Count);
        }
        [Test]
        public void Test_ObtenerTodasLosIngresos_DebeRetornarTodosLosIngresosMasDelTotal()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.GetAllIngreso();

            Assert.AreNotEqual(16, result.Count);
        }
        [Test]
        public void Test_ObtenerTodasLosIngresos_DebeRetornarTodosLosIngresosMenosDelTotal()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.GetAllIngreso();

            Assert.AreNotEqual(1, result.Count);
        }
        [Test]
        public void Test_CalculaSaldoSumadoConUnIngreso_DebeRetornarElSaldoMasIngresoCorrecto()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.calculaMontoConIngreso(100,20);

            Assert.AreEqual(120, result);
        }
        [Test]
        public void Test_CalculaSaldoSumadoConUnIngreso_DebeRetornarSaldoMasIngresoInCorrecto()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.calculaMontoConIngreso(100, 20);

            Assert.AreNotEqual(80, result);
        }

        [Test]
        public void Test_CalculaSaldoSumadoConUnGasto_DebeRetornarElSaldoMenosElGastoCorrecto()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.calculaMontoConGasto(100, 20);

            Assert.AreEqual(80, result);
        }
        [Test]
        public void Test_CalculaSaldoSumadoConUnGasto_DebeRetornarElSaldoMenosElGastoInCorrecto()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.calculaMontoConGasto(100, 20);

            Assert.AreNotEqual(120, result);
        }

        [Test]
        public void Test_ObtenerInstanciaDeCuenta_DebeRetornarObjetoDeLaClaseCuentaCorrecto()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.findCuentaByNombre("Cuenta 1");

            Assert.IsInstanceOf<Cuenta>(result);
        }
        [Test]
        public void Test_ObtenerCuentaPorNombreCuenta_NoDebeEncontrarLaCuenta()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.findCuentaByNombre("Cuenta 90");

            Assert.Null(result);
        }
        [Test]
        public void Test_ObtenerUnaListaDeCuentas_DebeRetornarListaDeCuentas()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.GetAllCuentas();

            Assert.IsInstanceOf<List<Cuenta>>(result);
        }
        [Test]
        public void Test_ObtenerUnaListaDeGasto_DebeRetornarListaDeGasto()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.GetAllGasto();

            Assert.IsInstanceOf<List<Gasto>>(result);
        }
        [Test]
        public void Test_ObtenerUnaListaDeIngreso_DebeRetornarListaDeIngreso()
        {
            var repository = new FinanzasRepository(mockContext.Object);

            var result = repository.GetAllIngreso();

            Assert.IsInstanceOf<List<Ingreso>>(result);
        }

    }
}
